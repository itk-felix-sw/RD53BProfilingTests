#include "RD53BEmulator/DataFrame.h"

#include <iostream>
#include <ctime>
#include <vector>
#include <iomanip>
#include <chrono>
#include <fstream>
#include <cmdl/cmdargs.h>
#include <TFile.h>
#include <TTree.h>

using namespace std;
using namespace RD53B;

int main(int argc, char ** argv) {
  
  cout << "#################################" << endl
       << "# profile_saskia_trees          #" << endl
       << "#################################" << endl;

  CmdArgBool cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgStr  cInfile(  'f',"outfile", "filename","input file",CmdArg::isREQ);
  CmdArgStr  cOutfile( 'o',"outfile", "filename","output file",CmdArg::isREQ);
  CmdArgBool cNoCid(   'C',"no-cid","turn off chip id");
  CmdArgBool cNoTot(   'T',"no-tot","turn off tot");
  CmdArgBool cNoBin(   'B',"no-bin","turn off binary tree");
  CmdArgBool cNoDec(   'D',"no-dec","turn off event decoding");
  CmdArgInt  cNum(     'n',"num","decodings", "number of decodings");

  cNum=1000;

  CmdLine cmdl(*argv,&cVerbose,&cInfile,&cOutfile,&cNoTot,&cNoBin,&cNoDec,&cNoCid,&cNum,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  string infile(cInfile);
  string outfile(cOutfile);
  int en_tot = !cNoTot;
  int en_bin = !cNoBin;
  int en_cid = !cNoCid;

  TFile * fr = TFile::Open(infile.c_str());
  TTree * tt = (TTree*)fr->Get("t");
  int32_t ndigis_on_det;
  int32_t px[705];
  int32_t py[705];
  tt->SetBranchAddress("ndigis_on_det", &ndigis_on_det);
  tt->SetBranchAddress("px", px);
  tt->SetBranchAddress("py", py);

  TFile * fw = TFile::Open(outfile.c_str(),"RECREATE");
  TTree * t2 = new TTree("timing","timing");

  float cpu_time;
  t2->Branch("NumTries",&cNum,"NumTries/I");
  t2->Branch("EnTot",&en_tot,"EnTot/I");
  t2->Branch("EnBin",&en_bin,"EnBin/I");
  t2->Branch("EnCid",&en_cid,"EnCid/I");
  t2->Branch("CpuTime",&cpu_time,"CpuTime/F");


  cout << "Create a new stream" << endl;
  Stream * stream = new Stream();
  stream->EnableEnc(!cNoBin);
  stream->EnableTot(!cNoTot);
  stream->EnableChipId(!cNoCid);
  vector<vector<uint32_t> > pixidx={{0,1,2,3,8,9,10,11},{4,5,6,7,12,13,14,15}};
  uint32_t hmaps[50][50];
  vector<uint8_t> bytes(1E7,0);
  vector<uint8_t> bytes2(1E7,0);

  for(uint32_t entry=0; entry<tt->GetEntries();entry++){

    cout << "Get Entry: " << entry << endl;
    tt->GetEntry(entry);

    stream->Clear();
    cout << "End stream clear" << endl;

    for(uint32_t i=0;i<50;i++){
      for(uint32_t j=0;j<50;j++){
        hmaps[i][j]=0;
      }
    }
    cout << "End map clear" << endl;

    for(int32_t hit=0; hit<ndigis_on_det; hit++){
      uint32_t qcol = (px[hit])/8;
      uint32_t qrow = (py[hit])/2;
      uint32_t hcol = (px[hit])%8;
      uint32_t hrow = (py[hit])%2;
      if(cVerbose){
        cout << " qrow: " << qrow
            << " qcol: " << qcol
            << " hrow: " << hrow
            << " hcol: " << hcol
            << endl;
      }
      if(qcol>50 || qrow>48){continue;}
      hmaps[qcol][qrow] |= (1<<pixidx[hrow][hcol]);
    }
    cout << "Adding quad core regions " << endl;
    for(uint32_t i=0;i<50;i++){
      for(uint32_t j=0;j<50;j++){
        if(hmaps[i][j]==0){continue;}
        stream->AddHit(7,i+1,j,hmaps[i][j],hmaps[i][j]);
      }
    }

    if(cVerbose){
      cout << stream->ToString() << endl;
    }

    cout << "Create a byte stream" << endl;
    bytes.clear();

    cout << "Encode" << endl;
    stream->Pack(&bytes[0]);

    cout << "Clear" << endl;
    stream->Clear();

    cout << "Decode" << endl;
    stream->UnPack(&bytes[0],bytes.size());

    if(!cVerbose){

      bytes2=bytes;

      cout << "Decode performance measurement" << endl;
      auto start = std::chrono::steady_clock::now();
      for(int32_t i=0; i<cNum; i++){
        stream->Clear();
        stream->UnPack(&bytes2[0],bytes2.size());
      }
      auto end = std::chrono::steady_clock::now();
      cpu_time = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
      float frequency = (float) cNum / cpu_time;
      cpu_time = cpu_time;

      cout << "NumTries       : " << cNum << endl;
      cout << "Enable Decoding: " << !cNoDec << endl;
      cout << "Enable ChipId  : " << !cNoCid << endl;
      cout << "Enable TOT     : " << !cNoTot << endl;
      cout << "Enable BinTree : " << !cNoBin << endl;
      cout << "CPU time  [s]  : " << fixed << setprecision(3) << cpu_time/1E6 << endl;
      cout << "Frequency [MHz]: " << fixed << setprecision(3) << frequency << endl;
    }
    t2->Fill();
  }
  
  cout << "Write output file: " << outfile << endl;
  t2->Write();
  fw->Close();
  fr->Close();


  cout << "Cleaning the house" << endl;
  bytes.clear();
  bytes2.clear();
  delete stream;
  delete fw;
  delete fr;

  cout << "Have a nice day" << endl;
  return 0;
}

